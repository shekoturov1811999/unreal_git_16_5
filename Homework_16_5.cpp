

#include <iostream>

int main()
{
	const int N = 12;
	int a[N][N], IndexSum = 4 % N, sum = 0;
	for (int i = 0; i < N; i++)
	{

		for (int j = 0; j < N; j++)
		{

			a[i][j] = { i + j };

			std::cout << a[i][j] << " ";

			if (i != IndexSum) 

				continue; 

			else
				
				sum += a[i][j];
			
		}
		std::cout << "\n" << std::endl;
	}
	std::cout <<  "Sum of elements at " << IndexSum << " array index is: " << 
		sum << std::endl;

	std::cout << "In ASCII standart code the symbol by this meaning will be: " << 
		static_cast<char>(sum) << std::endl;
	

}


